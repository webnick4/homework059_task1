import React, { PureComponent } from 'react';

class FilmsListItem extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isEditing: false
    };
  }

  renderFilm() {
    const {film, isCompleted } = this.props;

    const filmStyle = {
      color: isCompleted ? 'green' : 'red',
      cursor: 'pointer',
      padding: '5px'
    };

    if (this.state.isEditing) {
      return (
        <td>
          <form onClick={this.onSaveClick.bind(this)}>
            <input type="text" defaultValue={film} ref="editInput" />
          </form>
        </td>
      );
    }

    return (
      <td
        style={filmStyle}
        onClick={this.props.toggle.bind(this, film)}
      >
        {film}
      </td>
    );
  }

  renderActions() {

    if (this.state.isEditing) {
      return (
        <td>
          <button onClick={this.onSaveClick.bind(this)}>Save</button>
          <button onClick={this.onCancelClick.bind(this)}>Cancel</button>
        </td>
      );
    }

    return (
      <td>
        <button onClick={this.onEditClick.bind(this)}>Edit</button>
        <button onClick={this.props.delete.bind(this, this.props.film)}>Delete</button>
      </td>
    );
  }

  onEditClick() {
    this.setState({ isEditing: true });
  }

  onCancelClick() {
    this.setState({ isEditing: false });
  }

  onSaveClick(event) {
    event.preventDefault();

    const oldFilm = this.props.film;
    const newFilm = this.refs.editInput.value;
    this.props.save(oldFilm, newFilm);
    this.setState({ isEditing: false });
  }

  render() {
    return (
      <tr>
        {this.renderFilm()}
        {this.renderActions()}
      </tr>
    );
  }
}

export default FilmsListItem;