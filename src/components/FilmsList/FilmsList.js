import React, { Component } from 'react';
import FilmsListHeader from "./FilmsListHeader/FilmsListHeader";
import FilmsListItem from "./FilmsListItem/FilmsListItem";
import _ from 'lodash';
import './FilmsList.css'


class FilmsList extends Component {
  renderItems() {
    const props = _.omit(this.props, 'films');
    return _.map(this.props.films, (film, index) => <FilmsListItem key={index} {...film} {...props} />)
  }

  render() {
    return (
      <table className="FilmsList">
        <FilmsListHeader />
        <tbody>
          {this.renderItems()}
        </tbody>
      </table>
    );
  }
}

export default FilmsList;