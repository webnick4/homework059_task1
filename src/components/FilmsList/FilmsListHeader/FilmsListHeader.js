import React, { Component } from 'react';

class FilmsListHeader extends Component {
  render() {
    return (
      <thead>
        <tr>
          <th>Title</th>
          <th>Action</th>
        </tr>
      </thead>
    );
  }
}

export default FilmsListHeader;