import React, { PureComponent } from 'react';
import _ from 'lodash';
import './AddFilm.css';

class AddFilm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      error: null
    };
  }

  renderError() {
    if (!this.state.error) return null;

    return <div style={{color: 'red'}}>{this.state.error}</div>
  }

  handleCreate(event) {
    event.preventDefault();

    const addFilm = this.refs.addFilm;
    const film = addFilm.value;
    const validateInput = this.validateInput(film);

    if (validateInput) {
      this.setState({ error: validateInput });
      return;
    }

    this.setState({ error: null });
    this.props.add(film);
    this.refs.addFilm.value = '';
  }

  validateInput(movie) {
    if (!movie) {
      return 'Please enter the name of movie';
    } else if (_.find(this.props.films, el => el.film === movie)) {
      return 'Task already exists';
    } else {
      return null;
    }
  }

  render() {
    return (
      <form onSubmit={this.handleCreate.bind(this)} className="AddFilms-form">
        <input className="AddFilms-input" type="text" placeholder="What do you want to see?" ref="addFilm" />
        <button className="AddFilms-btn">Add</button>

        {this.renderError()}
      </form>
    );
  }
}

export default AddFilm;