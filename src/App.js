import React, { Component } from 'react';
import CatalogFilms from "./containers/CatalogFilms/CatalogFilms";

class App extends Component {
  render() {
    return <CatalogFilms/>;
  }
}

export default App;
