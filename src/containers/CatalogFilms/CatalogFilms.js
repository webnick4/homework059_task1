import React, { Component, Fragment } from 'react';
import FilmsList from "../../components/FilmsList/FilmsList";
import AddFilm from "../../components/AddFilm/AddFilm";
import _ from 'lodash';
import './CalalogFilms.css'


const films = [
  { film: 'Pulp Fiction', isCompleted: false },
  { film: 'Requiem for a Dream', isCompleted: true }
];

class CatalogFilms extends Component {
  constructor(props) {
    super(props);

    this.state = {
      films
    };
  }

  toggleFilm(film) {
    const foundFilm = _.find(this.state.films, el => el.film === film);
    foundFilm.isCompleted = !foundFilm.isCompleted;
    this.setState({ films: this.state.films });
  }

  addFilm(film) {
    this.state.films.push({film, isCompleted: false});
    this.setState({ films: this.state.films });
  }

  saveFilm(oldFilm, newFilm) {
    const foundFilm = _.find(this.state.films, el => el.film === oldFilm);
    foundFilm.film = newFilm;
    this.setState({ films: this.state.films });
  }

  deleteFilm(film) {
    _.remove(this.state.films, el => el.film === film);
    this.setState({ films: this.state.films });
  }

  render() {
    return (
      <Fragment>
        <div className="CatalogFilms">
          <div className="CatalogFilms-films">
            <h1>Catalog Of Films</h1>
            <AddFilm
              films={this.state.films}
              add={this.addFilm.bind(this)}
            />

            <FilmsList
              films={this.state.films}
              toggle={this.toggleFilm.bind(this)}
              save={this.saveFilm.bind(this)}
              delete={this.deleteFilm.bind(this)}
            />
          </div>
        </div>
      </Fragment>
    )
  }
}

export default CatalogFilms;